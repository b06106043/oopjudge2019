import java.lang.Integer;

/**
 * This class uses an Integer array as its internal instance variable to save
 * integer , and its size will changed automatically based on user's
 * manipulation.
 */
public class SimpleArrayList {
	public int size;
	public int initialsize;

	Integer[] ilist = new Integer[size];
	Integer[] temp = new Integer[10];

	/**
	 * default constructor to initialize the SimpleArraylist.
	 */
	public SimpleArrayList() {
		size = 0;
		for (int i = 0; i < ilist.length; i++) {
			ilist[i] = 0;
		}
	}

	/**
	 * constructor to set the value of the SimpleArraylist element.
	 */
	public SimpleArrayList(int input) {
		size = input;
		Integer[] ilist = new Integer[size];
		for (int i = 0; i < ilist.length; i++) {
			Integer o = 0;
			ilist[i] = o;
			temp[i] = o;
		}

	}

	/**
	 * appends the given Integer to the end of this array.
	 */
	public void add(Integer index) {
		size++;
		Integer[] ilist = new Integer[size];
		ilist[size - 1] = index;
		temp[size - 1] = ilist[size - 1];
		for (int i = 0; i < ilist.length; i++) {
			ilist[i] = temp[i];
		}
	}

	/**
	 * returns the element of the given position in this array
	 */
	public Integer get(int index) {
		if (index == 0 && size == 0 || index > size - 1)
			return null;
		else
			return temp[index];
	}

	/**
	 * replaces the element at the given position in this array with the given
	 * element, and returns the original element at that given position. If the
	 * given position is out of range of the array, returns null.
	 */
	public Integer set(int index, Integer element) {
		Integer origin;
		if (index > size - 1)
			return null;
		else
			origin = temp[index];
		temp[index] = element;
		return origin;

	}

	/**
	 * If a null element is at the given position, returns false; otherwise removes
	 * it and returns true. Shifts any subsequent elements to the left if removes
	 * successfully.
	 */
	public boolean remove(int index) {

		boolean flag = true;
		if (temp[index] == null) {
			flag = false;
			return flag;
		} else if (temp[index] >= 0 || temp[index] < 0)
			size--;
		for (int i = index; i < size; i++) {
			temp[i] = temp[i + 1];
		}
		return flag;
	}

	/**
	 * removes all of the elements from this array.
	 */
	public void clear() {

		size = 0;
	}

	/**
	 * returns the number of elements in this array.
	 */
	public int size() {

		return size;
	}

	/**
	 * retains only the elements in this array that are contained in the given
	 * SimpleArrayList. Removes from this array all of its elements
	 * that are not contained in the given SimpleArrayList. Returns true if this
	 * array changed as a result
	 */
	public boolean retainAll(SimpleArrayList two) {
		boolean flag = false;
		int counter = 0;
		Integer comparepos = 100;
		int[] count = new int[10];
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < two.size; j++) {
				if (two.temp[j] != temp[i]) {
					counter++;
					count[i] = counter;
				}
			}

			counter = 0;
			comparepos = 100;
		}
		for (int k = 0; k < size; k++) {
			if (count[k] == two.size) {
				temp[k] = 44;
			} else
				continue;
		}
		for (int l = 0; l < size; l++) {
			if (temp[l] == 44) {
				size--;
				for (int m = l; m < size; m++) {
					temp[m] = temp[m + 1];
					flag = true;
				}
			}
		}
		return flag;
	}

}
/**
 * test
 */
class arraydemo {
	public static void main(String[] args) {
		System.out.println("=== TASK 1 ===");
		SimpleArrayList list = new SimpleArrayList();
		System.out.println(list.get(0));
		System.out.println("=== TASK 2 ===");
		list.add(2);
		list.add(5);
		list.add(8);
		list.add(1);
		list.add(12);
		System.out.println(list.get(2));
		System.out.println("=== TASK 3 ===");
		System.out.println(list.get(5));
		System.out.println("=== TASK 4 ===");
		System.out.println(list.set(2, 100));
		System.out.println("=== TASK 5 ===");
		System.out.println(list.get(2));
		System.out.println("=== TASK 6 ===");
		System.out.println(list.set(5, 100));
		System.out.println("=== TASK 7 ===");
		System.out.println(list.remove(2));
		System.out.println("=== TASK 8 ===");
		System.out.println(list.get(2));
		System.out.println("=== TASK 9 ===");
		System.out.println(list.remove(2));
		System.out.println("=== TASK 10 ===");
		System.out.println(list.get(2));
		System.out.println("=== TASK 11 ===");
		System.out.println(list.get(3));
		System.out.println("=== TASK 12 ===");
		list.clear();
		System.out.println(list.get(0));
		System.out.println("=== TASK 13 ===");
		SimpleArrayList list2 = new SimpleArrayList(5);
		System.out.println(list2.get(3));
		System.out.println("=== TASK 14 ===");
		System.out.println(list2.get(9));
		System.out.println("=== TASK 15 ===");
		for (int i = 0; i < list2.size(); i++) {
			System.out.println(list2.set(i, i));
		}
		for (int i = 0; i < 5; i++) {
			list.add(i);
		}
		System.out.println(list.retainAll(list2));
		System.out.println("=== TASK 16 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
		System.out.println("=== TASK 17 ===");
		System.out.println(list2.remove(0));
		System.out.println(list2.remove(2));
		System.out.println(list.retainAll(list2));

		System.out.println("=== TASK 18 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 19 ===");
		System.out.println(list.set(1, null));
		System.out.println(list.remove(1));

		System.out.println("=== TASK 20 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 21 ===");
		System.out.println(list.set(1, 123));

		System.out.println("=== TASK 22 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 23 ===");
		System.out.println(list.remove(1));

		System.out.println("=== TASK 24 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 25 ===");
		list.add(null);
		System.out.println(list.remove(2));

		System.out.println("=== TASK 26 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
}