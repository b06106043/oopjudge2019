/**
 * this class allows up to three pizzas to be saved in an order.
 */
public class PizzaOrder extends Pizza{
	private int NumberPizzas;
	Pizza pizza1 = new Pizza();
	Pizza pizza2 = new Pizza();
	Pizza pizza3 = new Pizza();
	/**
	 * to check the input number exceed the max size 3 or not.And is also a setter to set the number value of pizza. 
	 */
	public boolean setNumberPizzas(int numberPizzas) {
		NumberPizzas =numberPizzas;
		return(numberPizzas>=1&&numberPizzas<=3);
	}
	/**
	 * setter
	 * @return
	 */
	public void setPizza1(Pizza pizza1) {
		this.pizza1 = pizza1;
	}
	/**
	 * setter
	 * @return
	 */
	public void setPizza2(Pizza pizza2) {
		this.pizza2 = pizza2;
	}
	/**
	 * setter
	 * @return
	 */
	public void setPizza3(Pizza pizza3) {
		this.pizza3 = pizza3;
	}
	/**
	 * to calculate the total price of the pizzas.
	 */
	public double calcTotal() {
		double sum=0;
		if(NumberPizzas==2.0) {
		sum = pizza1.calcCost()+pizza2.calcCost();
		}
		else if(NumberPizzas==3.0) {
		sum = pizza1.calcCost()+pizza2.calcCost()+pizza3.calcCost();
		}
		return sum;
	}
}
/**
 * test
 */
class pizzademo1 {
    public static void main(String[] args) {
    	Pizza pizza1 = new Pizza("large", 1, 0, 1);
    	Pizza pizza2 = new Pizza("medium", 2, 2, 5);
    	Pizza pizza3 = new Pizza();
    	PizzaOrder order = new PizzaOrder();
    	System.out.println(order.setNumberPizzas(5));
    	order.setNumberPizzas(2);
    	order.setPizza1(pizza1);
    	order.setPizza2(pizza2);
    	System.out.println(order.calcTotal());
    	order.setNumberPizzas(3);
    	order.setPizza1(pizza1);
    	order.setPizza2(pizza2);
    	order.setPizza3(pizza3);
    	System.out.println(order.calcTotal());
}
}