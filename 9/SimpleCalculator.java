import java.text.DecimalFormat;

/**
 * a simpleCalculator
 */
public class SimpleCalculator {
	public static int count = 0;
	private double result = 0.00;
	char operator;
	double value;
	char cvalue;
	static boolean flag;
	DecimalFormat df1 = new DecimalFormat("0.00");

	/**
	 * Doing the calculation according the string value it get,and if the format is
	 * not correspond to one operator one blank and double number value,this method
	 * will throw an UnknownCmdException by we customize.
	 * 
	 * @param cmd
	 * @throws UnknownCmdException
	 */
	public void calResult(String cmd) throws UnknownCmdException {
		count++;
		flag = false;

		String[] cmdarry = cmd.split("");
		if (cmdarry.length < 3) {
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		}
		String[] cmdarr = cmd.split(" ");
		if (cmdarr.length > 3) {
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		}

		String soperator = cmdarr[0];
		String svalue = cmdarr[1];
		operator = soperator.charAt(0);

		try {
			value = Double.parseDouble(svalue);
			if (operator != '+' && operator != '-' && operator != '*' && operator != '/') {
				throw new UnknownCmdException(operator + " is an unknown operator");
			} else if (value == 0) {
				throw new UnknownCmdException("Can not divide by 0");
			}
			switch (operator) {
			case '+':
				result += value;
				flag = true;
				break;
			case '-':
				result -= value;
				flag = true;
				break;
			case '*':
				result = result * value;
				flag = true;
				break;
			case '/':
				result = result / value;
				flag = true;
				break;
			case 'r':
				count = 999;
				flag = true;
			default:
				break;
			}
		} catch (NumberFormatException e) {

			if (operator != '+' && operator != '-' && operator != '*' && operator != '/') {
				throw new UnknownCmdException(
						operator + " is an unknown operator and " + svalue + " is an unknown value");
			} else
				throw new UnknownCmdException(svalue + " is an unknown value");

		}

	}

	/**
	 * @return the result after each calculation,if there throw any exception, and
	 *         won't run this method.
	 */
	public String getMsg() {
		String s = "";
		if (flag == true || count == 0) {
			switch (count) {
			case 0:
				s = "Calculator is on. Result = " + df1.format(result);
				break;
			case 1:
				s = "Result" + " " + operator + " " + df1.format(value) + " = " + df1.format(result) + ". New result = "
						+ df1.format(result);
				break;
			case 999:
				s = "Final result = " + df1.format(result);
				break;
			default:
				s = "Result" + " " + operator + " " + df1.format(value) + " = " + df1.format(result)
						+ ". Updated result = " + df1.format(result);
			}
		}

		return s;
	}

	/**
	 * This method is going to check whether the calculator end or not.According to
	 * the specific two string value 'R' or 'r',if it get each of this,will change
	 * the boolean flag to false end return to the main method to end this
	 * calculator program.
	 * @param cmd
	 * @return
	 */
	public boolean endCalc(String cmd) {
		if (cmd.equals("r")) {
			flag = true;
			count = 999;
		}

		return (cmd.equals("r") || cmd.equals("R"));
	}
	/**
	 * To test the method write below.
	 */
	static class test {
		public static void main(String[] args) {
			SimpleCalculator cal = new SimpleCalculator();
			String cmd = null;
			System.out.println(cal.getMsg());
			String cmd_str = "+ 5,- 2,* 5,/ 3,% 2,* D,X D,XD,, ,/ 1000000,/ 00.000,/ 0.000001,+ 1 + 1,- 1.66633,r R,r";
			String[] cmd_arr = cmd_str.split(",");
			for (int i = 0; i < cmd_arr.length; i++) {
				try {
					if (cal.endCalc(cmd_arr[i]))
						break;

					cal.calResult(cmd_arr[i]);
					if (flag == true || count == 0) {
						System.out.println(cal.getMsg());
					}

				} catch (UnknownCmdException e) {
					System.out.println(e.getMessage());
				}
			}
			System.out.println(cal.getMsg());
		}
	}
}