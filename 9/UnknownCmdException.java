/**
 * a customize exception according running the SimpleCalculator
 */
public class UnknownCmdException extends Exception{

	public UnknownCmdException(String errMessage) {
	
		 		super(errMessage);
		
	}
}
