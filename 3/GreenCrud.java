/**
 * a function to calculate the size of the green crud population after a given
 * amount of days, starting at a given initial size.
 */
public class GreenCrud {
	/**
	 * receive the given days and initial size,this method will calculate the response number of green crud
	 * @param initialSize
	 * @param days
	 * @return the calculated result
	 */
	public int calPopulation(int initialSize, int days) {
		int fi = days / 5 + 1;
		int[] num = new int[fi + 1];
		num[0] = 0;
		num[1] = 1;
		for (int i = 2; i < fi + 1; i++) {
			num[i] = num[i - 1] + num[i - 2];
		}
		return initialSize * (num[fi]);
	}
	/**
	 * test the method
	 */
	public static void main(String[] args) {
		int x = 100, y = 100;
		GreenCrud gc = new GreenCrud();
		gc.calPopulation(x, y);
		System.out.println(gc.calPopulation(6, 15));
		System.out.println(gc.calPopulation(10, 17));
		System.out.println(gc.calPopulation(3, 20));
	}

}
