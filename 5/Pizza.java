/**
 * this class stores information about a single pizza.
 */
public class Pizza {

	private String size;
	private int NumberofCheese;
	private int NumberofPepperoni;
	private int NumberofHam;
	/**
	 * default constructor to set the Pizza information
	 */
	public Pizza() {
		size = "small";
		NumberofCheese = 1;
		NumberofPepperoni = 1;
		NumberofHam = 1;
	}
	/**
	 * constructor to set the Pizza information,according the accept parameter 
	 * @param Size
	 * @param NumberofCheese
	 * @param NumberofPepperoni
	 * @param NumberofHam
	 */
	public Pizza(String Size, int NumberofCheese, int NumberofPepperoni, int NumberofHam) {
		setSize(Size);
		setNumberOfCheese(NumberofCheese);
		setNumberOfPepperoni(NumberofPepperoni);
		setNumberOfHam(NumberofHam);
	}
	/**
	 * getter
	 * @return
	 */
	public String getSize() {
		return size;
	}
	/**
	 * setter
	 * @return
	 */
	public void setSize(String size) {
		this.size = size;
	}
	/**
	 * getter
	 * @return
	 */
	public int getNumberOfCheese() {
		return NumberofCheese;
	}
	/**
	 * setter
	 * @return
	 */
	public void setNumberOfCheese(int numberofCheese) {
		NumberofCheese = numberofCheese;
	}
	/**
	 * getter
	 * @return
	 */
	public int getNumberOfPepperoni() {
		return NumberofPepperoni;
	}
	/**
	 * setter
	 * @return
	 */
	public void setNumberOfPepperoni(int numberofPepperoni) {
		NumberofPepperoni = numberofPepperoni;
	}
	/**
	 * getter
	 * @return
	 */
	public int getNumberOfHam() {
		return NumberofHam;
	}
	/**
	 * setter
	 * @return
	 */
	public void setNumberOfHam(int numberofHam) {
		NumberofHam = numberofHam;
	}
	/**
	 * a method to count the total price of each customize Pizza
	 * @return
	 */
	public double calcCost() {
		double sum = 0;
		if (size == "small") {
			sum = 10 + NumberofCheese * 2 + NumberofPepperoni * 2 + NumberofHam * 2;
		} else if (size == "medium") {
			sum = 12 + NumberofCheese * 2 + NumberofPepperoni * 2 + NumberofHam * 2;
		} else if (size == "large") {
			sum = 14 + NumberofCheese * 2 + NumberofPepperoni * 2 + NumberofHam * 2;
		}
		return sum;
	}

	@Override
	public String toString() {
		return "size = " + size + ", numOfCheese = " + NumberofCheese + ", numOfPepperoni = " + NumberofPepperoni
				+ ", numOfHam = " + NumberofHam;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + NumberofCheese;
		result = prime * result + NumberofHam;
		result = prime * result + NumberofPepperoni;
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (NumberofCheese != other.NumberofCheese)
			return false;
		if (NumberofHam != other.NumberofHam)
			return false;
		if (NumberofPepperoni != other.NumberofPepperoni)
			return false;
		if (size == null) {
			if (other.size != null)
				return false;
		} else if (!size.equals(other.size))
			return false;
		return true;
	}

}
/**
 *test the Function below work or not.
 */
class pizzademo {
	public static void main(String[] args) {
		Pizza pizza = new Pizza("large", 3, 1, 5);
		System.out.println(pizza.getSize());
		System.out.println(pizza.getNumberOfCheese());
		System.out.println(pizza.getNumberOfPepperoni());
		System.out.println(pizza.getNumberOfHam());
		pizza = new Pizza();
		pizza.setSize("medium");
		pizza.setNumberOfCheese(2);
		pizza.setNumberOfPepperoni(4);
		pizza.setNumberOfHam(1);
		System.out.println(pizza.getSize());
		System.out.println(pizza.getNumberOfCheese());
		System.out.println(pizza.getNumberOfPepperoni());
		System.out.println(pizza.getNumberOfHam());
		System.out.println(pizza.calcCost());
		System.out.println(pizza.toString());
		System.out.println(pizza.equals(new Pizza("large", 2, 4, 1)));
		System.out.println(pizza.equals(new Pizza()));
		System.out.println(pizza.equals(new Pizza("medium", 2, 4, 1)));
	}
}
