/**
 * this class is to test the given year whether a leap year or not.
 */
        public class IsLeapYear
        {	
        	/**
        	 * do the flow control to determine the given year a leap year or not. 
        	 * @param year
        	 * @return a boolean to main function that tell the user whether this year leap year or not.
        	 */
             public boolean determine(int year) 
                {
                	if((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))
                	return true;
                	else
                	return false;
                }
             public static void main(String[] args){	
            int x=0;
            IsLeapYear ily= new IsLeapYear();
            ily.determine(x); 
            System.out.println(ily.determine(2014));
            System.out.println(ily.determine(2004));
            System.out.println(ily.determine(2100));
             }
             
        }
