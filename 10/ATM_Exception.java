/**
 * a customize exception according running the ATM program
 */
public class ATM_Exception extends Exception {
	/**
	 * The Enum ExceptionTYPE contain two kinds of the exception will appear during
	 * the ATM program running.
	 */
	public enum ExceptionTYPE {

		BALANCE_NOT_ENOUGH, AMOUNT_INVALID;
	}

	private ExceptionTYPE excptionCondition;

	/**
	 * The setter of the self definition variable excptionConditon,according to the
	 * different exception throws during run the boolean method
	 * checkBalanceĦBisValidAmount.
	 * @param ex
	 */
	public ATM_Exception(ATM_Exception.ExceptionTYPE ex) {
		if (ex == ExceptionTYPE.BALANCE_NOT_ENOUGH) {
			excptionCondition = ExceptionTYPE.BALANCE_NOT_ENOUGH;
		} else
			excptionCondition = ExceptionTYPE.AMOUNT_INVALID;
	}
	/**
	 *A method return the error message .
	 */
	public String getMessage() {
		String error = excptionCondition.toString();
		return error;
	}
}
