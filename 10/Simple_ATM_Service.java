/**
 * A class implement the interface ATM_Service.Contain the method
 * checkBalanceĦBisValidAmount and withdraw.
 */
public class Simple_ATM_Service implements ATM_Service {
	/**
	 * A boolean method to check the given Account and the money user wanted to
	 * withdraw whether bigger than the user's balance or not,if don't,throw an
	 * ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH.
	 */
	public boolean checkBalance(Account account, int money) throws ATM_Exception {
		boolean balance = true;
		if (account.getBalance() < money) {
			balance = false;
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);
		}

		return balance;
	};

	/**
	 * A boolean method to check the given money user wanted to withdraw whether a
	 * multiple of 1000 or not,if don't,throw an
	 * ATM_Exception.ExceptionTYPE.AMOUNT_INVALID.
	 */
	public boolean isValidAmount(int money) throws ATM_Exception {
		boolean amount = true;
		if (money % 1000 != 0) {
			amount = false;
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
		}

		return amount;
	};

	/**
	 * A method to doing the withdraw feature.And will check the Balance and the
	 * request withdraw money valid or not,if these two boolean method both get the
	 * true result then will doing the withdraw operation.Else will catch the
	 * exception throw before and print the error message.
	 */
	public void withdraw(Account account, int money) {
		try {
			checkBalance(account, money);
			isValidAmount(money);
			if (checkBalance(account, money) && isValidAmount(money)) {
				account.setBalance(account.getBalance() - money);
			}

		} catch (ATM_Exception ex) {
			System.out.println(ex.getMessage());
		}
		System.out.println("updated balance : " + account.getBalance());
	};
}
/**
 * Test the method below.
 */
class test {
	public static void main(String[] args) {
		Account David = new Account(4000);
		Simple_ATM_Service atm = new Simple_ATM_Service();
		System.out.println("---- first withdraw ----");
		atm.withdraw(David, 1000);
		System.out.println("---- second withdraw ----");
		atm.withdraw(David, 1000);
		System.out.println("---- third withdraw ----");
		atm.withdraw(David, 1001);
		System.out.println("---- fourth withdraw ----");
		atm.withdraw(David, 4000);
	}
}
