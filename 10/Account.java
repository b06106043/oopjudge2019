/**
 * The given class the contain a Account's basic information-balance,and the
 * getter�Bsetter of this private integer variable.
 */
class Account {
	private int balance;

	public Account(int balance) {
		setBalance(balance);
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
}
