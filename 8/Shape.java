import java.lang.Math;
import java.text.DecimalFormat;

/**
 * the abstract class needs to be extend and implements all of its methods.
 */
public abstract class Shape {


	protected double length;

	public Shape(double length) {
		this.length = length;
	}

	public abstract void setLength(double length);

	public abstract double getArea();

	public abstract double getPerimeter();

	public String getInfo() {
		return "Area = " + getArea() + ", Perimeter = " + getPerimeter();
	}
}
