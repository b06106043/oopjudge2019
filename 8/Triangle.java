import java.text.DecimalFormat;

/**
 * this class will generate a triangle object.
 */
public class Triangle extends Shape {

	DecimalFormat df = new DecimalFormat("##.00");

	public Triangle(double length) {
		super(length);
		// TODO Auto-generated constructor stub
		this.length = length;
	}

	@Override
	public void setLength(double length) {
		// TODO Auto-generated method stub
		this.length = length;

	}

	@Override
	public double getArea() {
		// TODO Auto-generated method stub
		return Double.parseDouble(df.format(length * Math.sqrt(3.0) * length / 4));
	}

	@Override
	public double getPerimeter() {
		// TODO Auto-generated method stub
		return Double.parseDouble(df.format(length * 3));
	}

}
