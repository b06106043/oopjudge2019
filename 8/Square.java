import java.text.DecimalFormat;

/**
 * this class will generate a square object.
 */

public class Square extends Shape {

	DecimalFormat df = new DecimalFormat("##.00");

	public Square(double length) {
		super(length);
		// TODO Auto-generated constructor stub
		this.length = length;
	}

	@Override
	public void setLength(double length) {
		// TODO Auto-generated method stub
		this.length = length;
	}

	@Override
	public double getArea() {
		// TODO Auto-generated method stub
		return Double.parseDouble(df.format(length * length));
	}

	@Override
	public double getPerimeter() {
		// TODO Auto-generated method stub
		return Double.parseDouble(df.format(4 * length));
	}

}
